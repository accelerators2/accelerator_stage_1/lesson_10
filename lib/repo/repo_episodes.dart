// ignore_for_file: avoid_print

import 'package:flutter_app_1/dto/episode.dart';
import 'package:flutter_app_1/generated/l10n.dart';
import 'package:flutter_app_1/repo/api.dart';

class RepoEpisodes {
  RepoEpisodes({required this.api});

  final Api api;

  Future<ResultRepoEpisodes> fetch() => nextPage(1);

  Future<ResultRepoEpisodes> nextPage(int page) async {
    try {
      final result = await api.dio.get('/episode?page=$page');

      //если поле [next] не содержит ничего, то больше страниц нет
      final bool isEndOfData = result.data['info']['next'] == null;

      //преобразуем список сырых данных с сервера в нужный нам формат
      final List json = result.data['results'] ?? [];
      final list = json.map((item) => Episode.fromJson(item));
      return ResultRepoEpisodes(
        episodes: list.toList(),
        isEndOfData: isEndOfData,
      );
    } catch (error) {
      print('🏐 Error : $error');
      return ResultRepoEpisodes(
        errorMessage: S.current.somethingWentWrong,
      );
    }
  }
}

class ResultRepoEpisodes {
  ResultRepoEpisodes({
    this.errorMessage,
    this.episodes,
    this.isEndOfData = false,
  });

  final List<Episode>? episodes;
  final String? errorMessage;
  final bool isEndOfData;
}
